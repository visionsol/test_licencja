using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Xml;

namespace LicenseSigner
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class SignerForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.OpenFileDialog m_openCertDlg;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		private SignatureCreator m_creator = new SignatureCreator();

		public SignerForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.m_openCertDlg = new System.Windows.Forms.OpenFileDialog();
			// 
			// m_openCertDlg
			// 
			this.m_openCertDlg.DefaultExt = "lic";
			this.m_openCertDlg.Filter = "License files|*.lic";
			// 
			// LicenseSigner
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 266);
			this.Name = "LicenseSigner";
			this.Text = "License Signer";
			this.Load += new System.EventHandler(this.LicenseSigner_Load);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new SignerForm());
		}

		private void LicenseSigner_Load(object sender, System.EventArgs e)
		{
			if(m_openCertDlg.ShowDialog() == DialogResult.OK)
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(m_openCertDlg.FileName);
				string text = doc.DocumentElement["driveVolumeNumber"].InnerText+
					doc.DocumentElement["cpuId"].InnerText+
					doc.DocumentElement["mac"].InnerText+
					doc.DocumentElement["expires"].InnerText;
				doc.DocumentElement["signature"].InnerText = m_creator.Sign(text);
				doc.Save(m_openCertDlg.FileName);
				MessageBox.Show(this, "Licencja zosta�a podpisana");
			}
			Close();
		}
	}
}

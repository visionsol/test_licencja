using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Reflection;
using System.Resources;

namespace LicenseSigner
{
	/// <summary>
	/// Summary description for SignatureCreator.
	/// </summary>
	public class SignatureCreator
	{
		private DSA m_dsa;
		private SHA1 m_sha1;

		public SignatureCreator()
		{
			Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("LicenseSigner.privateKey.xml");
			StreamReader reader = new StreamReader(stream);
			string privateKeyXml = reader.ReadToEnd();
			m_dsa = new DSACryptoServiceProvider();
			m_dsa.FromXmlString(privateKeyXml);
			m_sha1 = new SHA1CryptoServiceProvider();
		}

		public string Sign(string text)
		{
			byte[] textb = Encoding.ASCII.GetBytes(text);
			byte[] hash = m_sha1.ComputeHash(textb);
			byte[] signatureb = m_dsa.CreateSignature(hash);
			return Convert.ToBase64String(signatureb);
		}
	}
}
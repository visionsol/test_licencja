using System;
using System.Collections;
using System.Management;

namespace LicenseValidator
{
	/// <summary>
	/// Summary description for Computer.
	/// </summary>
	public class Computer
	{
		private static string m_driveVolumeNumber, m_cpuId, m_mac;

		private Computer()
		{
		}

		public static string DriveVolumeNumber
		{
			get
			{
				if(m_driveVolumeNumber == null)
				{
					ManagementObject disk = new ManagementObject("Win32_LogicalDisk.deviceid=\"c:\"");
					disk.Get();
					m_driveVolumeNumber = disk["VolumeSerialNumber"].ToString();
				}
				return m_driveVolumeNumber;
			}
		}

		public static string CpuId
		{
			get
			{
				if(m_cpuId == null)
				{
					ManagementClass cpuClass = new ManagementClass("Win32_Processor");
					ManagementObjectCollection cpus = cpuClass.GetInstances();
					IEnumerator e = cpus.GetEnumerator();
					if(e.MoveNext())
					{
						ManagementObject cpu = (ManagementObject)e.Current;
						cpu.Get();
						m_cpuId = cpu["ProcessorId"].ToString();
					}
				}
				return m_cpuId;
			}
		}

		public static string Mac
		{
			get
			{
				if(m_mac == null)
				{
					ManagementClass adapterClass = new ManagementClass("Win32_NetworkAdapterConfiguration");
					ManagementObjectCollection adapters = adapterClass.GetInstances();
					foreach(ManagementObject adapter in adapters)
					{
						adapter.Get();
						if((bool)adapter["IPEnabled"])
						{
							m_mac = adapter["MacAddress"].ToString().Replace(":", "");
							break;
						}
					}
				}
                return m_mac;
			}
		}
	}
}

using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Reflection;
using System.Resources;

namespace LicenseValidator
{
	/// <summary>
	/// Summary description for SignatureVerifier.
	/// </summary>
	public class SignatureVerifier
	{
		private DSA m_dsa;
		private SHA1 m_sha1;

		public SignatureVerifier()
		{
			Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("LicenseValidator.publicKey.xml");
			StreamReader reader = new StreamReader(stream);
			string publicKeyXml = reader.ReadToEnd();
			m_dsa = new DSACryptoServiceProvider();
			m_dsa.FromXmlString(publicKeyXml);
			m_sha1 = new SHA1CryptoServiceProvider();
		}

		public bool Verify(string text, string signature)
		{
			byte[] textb = Encoding.ASCII.GetBytes(text);
			byte[] signatureb = Convert.FromBase64String(signature);
			if(signatureb.Length != 40)
				return false;
			byte[] hash = m_sha1.ComputeHash(textb);
			return m_dsa.VerifySignature(hash, signatureb);
		}
	}
}


using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Xml;
using System.Text;
using System.Windows.Forms;
using System.Data;

namespace LicenseValidator
{
	/// <summary>
	/// Summary description for Form1.
	/// </summary>
	public class ValidatorForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox m_realDiskVolume;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox m_realCpuId;
		private System.Windows.Forms.TextBox m_certDiskVolume;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox m_certCpuId;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox m_realMac;
		private System.Windows.Forms.TextBox m_certMac;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button m_newCert;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button m_browse;
		private System.Windows.Forms.SaveFileDialog m_saveCertDlg;
		private System.Windows.Forms.DateTimePicker m_expiry;
		private System.Windows.Forms.OpenFileDialog m_openCertDlg;
		private System.Windows.Forms.CheckBox m_valid;
		private System.Windows.Forms.TextBox m_certExpires;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private SignatureVerifier m_verifier = new SignatureVerifier();

		public ValidatorForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if (components != null) 
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.m_realDiskVolume = new System.Windows.Forms.TextBox();
			this.m_realCpuId = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.m_certDiskVolume = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.m_certCpuId = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.m_realMac = new System.Windows.Forms.TextBox();
			this.m_certMac = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.m_certExpires = new System.Windows.Forms.TextBox();
			this.m_valid = new System.Windows.Forms.CheckBox();
			this.m_newCert = new System.Windows.Forms.Button();
			this.m_expiry = new System.Windows.Forms.DateTimePicker();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.label8 = new System.Windows.Forms.Label();
			this.m_browse = new System.Windows.Forms.Button();
			this.m_saveCertDlg = new System.Windows.Forms.SaveFileDialog();
			this.m_openCertDlg = new System.Windows.Forms.OpenFileDialog();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.m_realMac);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.m_realCpuId);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.m_realDiskVolume);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(200, 136);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Stan rzeczywisty";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.m_browse);
			this.groupBox2.Controls.Add(this.m_certExpires);
			this.groupBox2.Controls.Add(this.label7);
			this.groupBox2.Controls.Add(this.m_certMac);
			this.groupBox2.Controls.Add(this.m_certDiskVolume);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.m_certCpuId);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Location = new System.Drawing.Point(216, 8);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(200, 208);
			this.groupBox2.TabIndex = 2;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Certyfikat";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(144, 23);
			this.label1.TabIndex = 0;
			this.label1.Text = "Drive C: Volume Number:";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(144, 23);
			this.label2.TabIndex = 0;
			this.label2.Text = "Drive C: Volume Number:";
			// 
			// m_realDiskVolume
			// 
			this.m_realDiskVolume.Location = new System.Drawing.Point(8, 32);
			this.m_realDiskVolume.Name = "m_realDiskVolume";
			this.m_realDiskVolume.ReadOnly = true;
			this.m_realDiskVolume.Size = new System.Drawing.Size(184, 20);
			this.m_realDiskVolume.TabIndex = 1;
			this.m_realDiskVolume.Text = "";
			// 
			// m_realCpuId
			// 
			this.m_realCpuId.Location = new System.Drawing.Point(8, 72);
			this.m_realCpuId.Name = "m_realCpuId";
			this.m_realCpuId.ReadOnly = true;
			this.m_realCpuId.Size = new System.Drawing.Size(184, 20);
			this.m_realCpuId.TabIndex = 2;
			this.m_realCpuId.Text = "";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 56);
			this.label3.Name = "label3";
			this.label3.TabIndex = 3;
			this.label3.Text = "1st CPU ID:";
			// 
			// m_certDiskVolume
			// 
			this.m_certDiskVolume.Location = new System.Drawing.Point(8, 32);
			this.m_certDiskVolume.Name = "m_certDiskVolume";
			this.m_certDiskVolume.ReadOnly = true;
			this.m_certDiskVolume.Size = new System.Drawing.Size(184, 20);
			this.m_certDiskVolume.TabIndex = 1;
			this.m_certDiskVolume.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(8, 56);
			this.label4.Name = "label4";
			this.label4.TabIndex = 3;
			this.label4.Text = "1st CPU ID:";
			// 
			// m_certCpuId
			// 
			this.m_certCpuId.Location = new System.Drawing.Point(8, 72);
			this.m_certCpuId.Name = "m_certCpuId";
			this.m_certCpuId.ReadOnly = true;
			this.m_certCpuId.Size = new System.Drawing.Size(184, 20);
			this.m_certCpuId.TabIndex = 2;
			this.m_certCpuId.Text = "";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(8, 96);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(144, 23);
			this.label6.TabIndex = 5;
			this.label6.Text = "MAC karty sieciowej:";
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 96);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(144, 23);
			this.label5.TabIndex = 5;
			this.label5.Text = "MAC karty sieciowej:";
			// 
			// m_realMac
			// 
			this.m_realMac.Location = new System.Drawing.Point(8, 112);
			this.m_realMac.Name = "m_realMac";
			this.m_realMac.ReadOnly = true;
			this.m_realMac.Size = new System.Drawing.Size(184, 20);
			this.m_realMac.TabIndex = 6;
			this.m_realMac.Text = "";
			// 
			// m_certMac
			// 
			this.m_certMac.Location = new System.Drawing.Point(8, 112);
			this.m_certMac.Name = "m_certMac";
			this.m_certMac.ReadOnly = true;
			this.m_certMac.Size = new System.Drawing.Size(184, 20);
			this.m_certMac.TabIndex = 6;
			this.m_certMac.Text = "";
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(8, 136);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(100, 16);
			this.label7.TabIndex = 7;
			this.label7.Text = "Licencja up造wa:";
			// 
			// m_certExpires
			// 
			this.m_certExpires.Location = new System.Drawing.Point(8, 152);
			this.m_certExpires.Name = "m_certExpires";
			this.m_certExpires.ReadOnly = true;
			this.m_certExpires.Size = new System.Drawing.Size(184, 20);
			this.m_certExpires.TabIndex = 8;
			this.m_certExpires.Text = "";
			// 
			// m_valid
			// 
			this.m_valid.Enabled = false;
			this.m_valid.Location = new System.Drawing.Point(16, 152);
			this.m_valid.Name = "m_valid";
			this.m_valid.Size = new System.Drawing.Size(184, 24);
			this.m_valid.TabIndex = 3;
			this.m_valid.Text = "Licencja wa積a";
			// 
			// m_newCert
			// 
			this.m_newCert.Location = new System.Drawing.Point(216, 32);
			this.m_newCert.Name = "m_newCert";
			this.m_newCert.Size = new System.Drawing.Size(96, 23);
			this.m_newCert.TabIndex = 4;
			this.m_newCert.Text = "Wygeneruj...";
			this.m_newCert.Click += new System.EventHandler(this.m_newCert_Click);
			// 
			// m_expiry
			// 
			this.m_expiry.Location = new System.Drawing.Point(8, 32);
			this.m_expiry.Name = "m_expiry";
			this.m_expiry.TabIndex = 6;
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.m_newCert);
			this.groupBox3.Controls.Add(this.m_expiry);
			this.groupBox3.Controls.Add(this.label8);
			this.groupBox3.Location = new System.Drawing.Point(8, 224);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(408, 64);
			this.groupBox3.TabIndex = 7;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Nowy certyfikat";
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(8, 16);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(160, 23);
			this.label8.TabIndex = 7;
			this.label8.Text = "Data up造wu licencji:";
			// 
			// m_browse
			// 
			this.m_browse.Location = new System.Drawing.Point(8, 176);
			this.m_browse.Name = "m_browse";
			this.m_browse.Size = new System.Drawing.Size(112, 23);
			this.m_browse.TabIndex = 9;
			this.m_browse.Text = "Plik certyfikatu...";
			this.m_browse.Click += new System.EventHandler(this.m_browse_Click);
			// 
			// m_saveCertDlg
			// 
			this.m_saveCertDlg.DefaultExt = "lic";
			this.m_saveCertDlg.Filter = "License files|*.lic";
			// 
			// m_openCertDlg
			// 
			this.m_openCertDlg.DefaultExt = "lic";
			this.m_openCertDlg.Filter = "License files|*.lic";
			// 
			// ValidatorForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(424, 294);
			this.Controls.Add(this.groupBox3);
			this.Controls.Add(this.m_valid);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Name = "ValidatorForm";
			this.Text = "Walidator Licencji";
			this.Load += new System.EventHandler(this.ValidatorForm_Load);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() 
		{
			Application.Run(new ValidatorForm());
		}

		private void ValidatorForm_Load(object sender, System.EventArgs e)
		{
			m_realDiskVolume.Text = Computer.DriveVolumeNumber;
			m_realCpuId.Text = Computer.CpuId;
			m_realMac.Text = Computer.Mac;
		}

		private void m_newCert_Click(object sender, System.EventArgs e)
		{
			if(m_saveCertDlg.ShowDialog(this) == DialogResult.OK)
			{
                XmlTextWriter writer = new XmlTextWriter(m_saveCertDlg.FileName, Encoding.UTF8);
				writer.Formatting = Formatting.Indented;
				writer.Indentation = 2;
				writer.IndentChar = ' ';
				writer.WriteStartDocument();
				writer.WriteStartElement("certificate");
				writer.WriteElementString("driveVolumeNumber", Computer.DriveVolumeNumber);
				writer.WriteElementString("cpuId", Computer.CpuId);
				writer.WriteElementString("mac", Computer.Mac);
				writer.WriteElementString("expires", m_expiry.Value.ToShortDateString());
				writer.WriteElementString("signature", "");
				writer.Close();
			}
		}

		private void m_browse_Click(object sender, System.EventArgs e)
		{
			if(m_openCertDlg.ShowDialog(this) == DialogResult.OK)
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(m_openCertDlg.FileName);
				m_certDiskVolume.Text = doc.DocumentElement["driveVolumeNumber"].InnerText;
				m_certCpuId.Text = doc.DocumentElement["cpuId"].InnerText;
				m_certMac.Text = doc.DocumentElement["mac"].InnerText;
				m_certExpires.Text = doc.DocumentElement["expires"].InnerText;
				string signature = doc.DocumentElement["signature"].InnerText;
				m_valid.Checked = false;
				if(doc.DocumentElement["driveVolumeNumber"].InnerText != Computer.DriveVolumeNumber)
				{
					MessageBox.Show(this, "Nieprawid這wy Drive Volume Number.");
					return;
				}
				if(doc.DocumentElement["cpuId"].InnerText != Computer.CpuId)
				{
					MessageBox.Show(this, "Nieprawid這wy CPU ID.");
					return;
				}
				if(doc.DocumentElement["mac"].InnerText != Computer.Mac)
				{
					MessageBox.Show(this, "Nieprawid這wy CPU ID.");
					return;
				}
				DateTime date = DateTime.Parse(doc.DocumentElement["expires"].InnerText);
				if(date < DateTime.Now)
				{
					MessageBox.Show(this, "Czas wa積o�ci licencji up造n像.");
					return;
				}
				string text = doc.DocumentElement["driveVolumeNumber"].InnerText+
					doc.DocumentElement["cpuId"].InnerText+
					doc.DocumentElement["mac"].InnerText+
					doc.DocumentElement["expires"].InnerText;
				if(!m_verifier.Verify(text, doc.DocumentElement["signature"].InnerText))
				{
					MessageBox.Show(this, "Brak prawid這wej sygnatury.");
					return;
				}
				MessageBox.Show("Licencja wa積a.");
				m_valid.Checked = true;
			}
		}
	}
}

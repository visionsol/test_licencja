using System;
using System.IO;
using System.Security.Cryptography;

namespace KeyGen
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	class KeyGen
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main(string[] args)
		{
			DSA dsa = new DSACryptoServiceProvider();
			StreamWriter writer = new StreamWriter("privateKey.xml");
			writer.WriteLine(dsa.ToXmlString(true));
			writer.Close();
			writer = new StreamWriter("publicKey.xml");
			writer.WriteLine(dsa.ToXmlString(false));
			writer.Close();
		}
	}
}
